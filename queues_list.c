#include<stdio.h>
#include"emulate.h"
#include<stdbool.h>
#include<stdlib.h>

void add_item_list(s_queue *queue,void *packet)
{

      s_list *first = queue->first;
      s_list *last  = queue->last;

      /* Create a new element */
      s_list *newNode = (s_list *)malloc(sizeof(s_list));
      newNode->packet = packet;
      newNode->next = NULL;
      /* If the list is empty */
      if(!first)
      {
           //printf(" List is empty,create first and last\n");
           first = newNode;
           last = newNode;
           queue->first = first; 
           queue->last = last; 
      }
      else
      {
           /* Add data at the last */
 	   last->next = newNode;
           last = newNode;
           queue->first = first; 
           queue->last = last; 
      }
#if 0 // debug 
      s_list *current = first;
      printf("list items are \n");
      while(current != NULL)
      {
          s_packet *info = (s_packet *)current->packet;
          printf("List :: %d\n",info->packet_num);
          current = current->next; 
      }
#endif
}


void delete_item_list(s_queue *queue,void *packet)
{
	s_list *first = queue->first;
	s_list *last  = queue->last;

       /* if the list is empty */
	if(first == NULL)
		return;
        /* Item to be deleted is the first */
	if(first->packet == packet)
	{
              s_list *temp = first;
              first = first->next;
              free(temp);
	}
	else
	{
              s_list *current = first->next;
              s_list *trailCurrent = first;
              bool found = false;
              while(current != NULL && found != true) 
	      {
		      if(current->packet == packet)
		      {
			      found = true;
			      break;
		      }
                      trailCurrent = current; 
                      current = current->next; 
	      }
              
              if(found == true)
	      {
                  /* Item to be removed is the last element */
                  if(current == last)
                  {
                       last = trailCurrent;
                       last->next = NULL;
                       free(current);
		  }
		  else
		  {
                       trailCurrent->next = current->next;
                       free(current);
		  } 
	      }


	}

}

s_packet* remove_element_first(s_queue *queue)
{
	s_list *first = queue->first;
	s_list *last  = queue->last;
	/* Remove item from first */ 
	if(first != NULL)
	{
		s_list *current = first;
		first = first->next;

		s_packet *packet = current->packet;
		free(current);
		queue->first = first; 
		queue->last = last; 
		return packet;
	}
	return NULL;
}

int isListEmpty(s_queue *queue)
{
   if(!queue->first)
    {
      return 1;
    }else
      return 0;
}

#if 0
void print_all_elements()
{
     s_list *current = first;
     while(current != NULL)
     {
	     //printf("%d\n",current->data);
             current = current->next;
     }
}
#endif
