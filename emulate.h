
#include <sys/time.h>
#include <math.h>
#include "stdlib.h"

typedef struct packet{
	unsigned int arr_time;
	int tokens_reqd;
	int packet_num;
	unsigned int servicetime;
        double Q1_time_enters;
        double total_Q1_time;
        double Q2_time_enters;
        double total_Q2_time;
      /*service time of a packet */
        double S_time_enters;
 /*total service time of a packet */
        double total_S_time;
}s_packet;



typedef struct cmd_line{
	double lambda;  //interarrival time
	double mu;     //service time of the packet
	double rate;       //rate at which tokens are generated 
	int depth;           //token depth
	int tokens;            //number of tokens required for each packet
}s_cmd_line;


typedef struct list{
	void *packet;
	struct list *next;
}s_list;

typedef struct packet_queue{
   s_list *first;
   s_list *last;
}s_queue;

/* Function Declarations */

void add_item_list(s_queue *queue,void *packet);
void delete_item_list(s_queue *queue,void *packet);
int isListEmpty(s_queue *queue);
void print_all_elements();
s_packet* remove_element_first(s_queue *queue);
