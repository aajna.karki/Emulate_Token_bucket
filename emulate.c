#include <stdio.h>
#include "emulate.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

/* Populate the command line parameters in this structure */
s_cmd_line params;

/* Queue declarations */
s_queue tx_queue;
s_queue rx_queue;

/*ctrl c interrpt signal*/
int cntrl_c_signal = 0;
sigset_t intSignal;

/*havent used in code yet*/
int totalPacketsToBeServed = 20;

/*total packets server from transmitter to receiver*/
int packet_served = 0;
int packets_drop = 0;
int total_packets_prod =0;

int tokens_drop = 0;
int total_tokens_prod = 0;

/*total inter arrival time*/
double total_i_a_time = 0;
/*total service time of the packet*/
double total_ser_time = 0;
/*total systems time of the packet*/
double total_sys_time = 0;
double time_in_Q1 =0.0;
double time_in_Q2 =0.0;
int packet_arrived = 0;
double total_ms = 0.0;
double sys_variance ;
double emulation_ends = 0.0;

/*pthread ID */
pthread_t token_threadId, transmitter_threadId, receiver_threadId, signalHandle;
bool ready = false;

/* pthread mutex initialization */
pthread_mutex_t lock_queue = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_wait = PTHREAD_COND_INITIALIZER;

int token_added = 0;

bool is_trasmitter_packets_ready()
{
     return ready;
}


/*API to calculate time in ms at which simulation begins*/
void InitialCurTimeOfDay(){
    int errno = -1;
    double s=0.0;
    float micro_s = 0;
    struct timeval mytime;
    if((errno = gettimeofday(&mytime, NULL))){
        fprintf(stderr, "PrintTimeOfDay %s",strerror(errno));
    }
    
    micro_s = (float)mytime.tv_usec/1000;
    s=(double)mytime.tv_sec*1000;
    
    total_ms = s + micro_s;
    
}
/*get current additional time taken from the start of the program in millseconds */
double GetTimeOfDay(){
    int errno = -1;
    double cur_time =0.0;
    double secs=0.0;
    double micro_s =0;
    
    struct timeval mytime;
    
    if((errno = gettimeofday(&mytime, NULL))){
        fprintf(stderr, "PrintTimeOfDay %s",strerror(errno));
    }
    
    micro_s = (double)mytime.tv_usec/1000;
    secs=(double)mytime.tv_sec*1000;

    /* get time in milliseconds*/
    cur_time = secs + micro_s - total_ms;
    
    return cur_time;
    
}


void* interrupt_handler()
{
   printf("\n\n %012.3lfms: ***** interrupt_handler thread  *****\n\n",GetTimeOfDay());
   int sig;
   sigwait(&intSignal, &sig);
   printf("\n\n %012.3lfms: ***** received interrupt_handler *****\n\n",GetTimeOfDay());
   cntrl_c_signal = 1;
   pthread_mutex_unlock(&lock_queue);
   pthread_cancel(transmitter_threadId);
   pthread_cancel(token_threadId);
   pthread_cancel(receiver_threadId);
   
   
}

void PrintStatistics(){
    float token_prob = 0.0, packet_prob=0.0;
    float stand_dev =0;

    fprintf(stdout, "\n-------------------Statistics:-----------------------\n");    

    if(total_packets_prod == 0){
        printf( "\tAverage packet inter-arrival time = N/A : No packets produced\n");
    }else{
        printf("\tAverage packet inter-arrival time = %.6g secs\n",total_i_a_time/total_packets_prod/1000); 
    }
    if (packet_served == 0) {
        printf("\tAverage packet service time = N/A : No packets arrived/served\n\n"); 
    }else{
        printf("\tAverage packet service time = %.6g secs\n\n",total_ser_time/packet_served/1000);
    }
    
    if (emulation_ends == 0) {
        printf("\tAverage number of packets in Q1 = N/A : emulation ends at invalid time\n");
        printf("\tAverage number of packets in Q2 = N/A : emulation ends at invalid time\n");
        printf("\tAverage number of packets in S = N/A : emulation ends at invalid time\n");
        
    }else{
        printf( "\tAverage number of packets in Q1 = %.6g\n",time_in_Q1/emulation_ends);
        printf( "\tAverage number of packets in Q2 = %.6g\n",time_in_Q2/emulation_ends);
        printf( "\tAverage number of packets at S = %.6g\n\n",total_ser_time/emulation_ends);
    }
    
    if (packet_served == 0) {
        printf( "\tAverage time a packet spent in system = N/A : No packets arrived/served\n");
        printf("\tStandard deviation for time spent in system = N/A : No packets arrived/served\n\n");
        
    }else{
        printf("\tAverage time a packet spent in system = %.6g secs\n",total_sys_time/packet_served/1000);
        stand_dev = sqrt((sys_variance/packet_served) - (total_sys_time/packet_served * total_sys_time/packet_served));
        
        printf("\tStandard deviation for time spent in system = %.6g \n\n",stand_dev/1000);
        
    }
    
    
    if(total_tokens_prod){
        token_prob = (float)tokens_drop/total_tokens_prod;
        printf("\tToken drop probability = %.6g\n",token_prob); 
    }
    else{
        fprintf(stdout,"\tToken drop probability = N/A : No tokens arrived\n");
    }
    
    
    if(total_packets_prod){
        packet_prob = (float)packets_drop/total_packets_prod;
        fprintf(stdout, "\tPacket drop probability = %.6g\n\n",packet_prob); 
    }
    else{
        fprintf(stdout,"\tPacket drop probability = N/A : No packets arrived\n\n");
    }
    
    
}
/************************************************************
Function    : token_thread 
 
Description : thread that handles creating tokens and putting it in 
              output queue 

************************************************************/
void* token_thread()
{
    printf("\n %012.3lfms: Token_thread created\n",GetTimeOfDay());
    float token_arrival_time = (1/params.rate);
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
    
    while(!cntrl_c_signal)
    {

	    /* sleep to simulate inter-arrival time */
            usleep(token_arrival_time * 1000000);
	    pthread_mutex_lock(&lock_queue);
	    /* Critical_section */

	    if(token_added < params.depth)
	    {
		    token_added++;
                    total_tokens_prod++;    
		    printf("\n %012.3lfms: Token Buckets has %d tokens\n",GetTimeOfDay(),token_added);
	    }
	    else
	    {
                    total_tokens_prod++;
                    tokens_drop++;
		    printf("\n %012.3lfms: Token dropped,token bucket has %d tokens\n",GetTimeOfDay(),token_added);
	    }
	    if(is_trasmitter_packets_ready())
	    {
		    if(params.tokens <= token_added)
		    {
                           printf("\n %012.3lfms: %d Tokens available for packet transmission\n",GetTimeOfDay(),token_added);
			    s_packet *packet_info = remove_element_first(&tx_queue);
                            packet_info->total_Q1_time = GetTimeOfDay() - packet_info->Q1_time_enters;
			    if(packet_info != NULL)
			    {	    
				   /*time during which packet enters q2*/
				    packet_info->Q2_time_enters = GetTimeOfDay();
				    printf("\n %012.3lfms: Adding Packet Number %d to receiver queue\n",GetTimeOfDay(),packet_info->packet_num);
				    add_item_list(&rx_queue,packet_info);
				    pthread_cond_broadcast(&cond_wait);
				    /* packet sent to receiver thread */
				    token_added = token_added - params.tokens;
			    }
		    }             

	    }
	    
	    pthread_mutex_unlock(&lock_queue);
    }
}

/************************************************************
Function    : transmitter_thread 
 
Description : thread that handles creating packts and putting it from 
              input queue to output queue

************************************************************/

void* transmitter_thread()
{
    printf("\n %012.3lfms: Transmitter_thread created\n",GetTimeOfDay());
  
    int packet_count = 0;
    double prev_itime = 0 ;
    double inter_a_time = 0.0;
 
    while(!cntrl_c_signal)
    {
    	/* Sleep to simulate inter-packet time */
    	usleep((1/params.lambda) * 1000000);
	
        inter_a_time = GetTimeOfDay()- prev_itime;
        
        prev_itime = GetTimeOfDay() ;
        
        total_i_a_time += inter_a_time;
  
    	/* Allocate memory for packet - Generate packet */
    	s_packet *transmit_packet = (s_packet*)malloc(sizeof(s_packet));
    	packet_count++;
        transmit_packet->servicetime = (1/params.mu*1000000);
    	/* Populate the transmit packet */
    	transmit_packet->tokens_reqd = params.tokens;
    	transmit_packet->packet_num = packet_count;

    		/* Check if there are enough tokens to transmit the packet */
    		if(transmit_packet->tokens_reqd <= params.depth)
    		{
			/* Enter critical section to write to the queues */
			if(!pthread_mutex_lock(&lock_queue))
			{
                                packet_arrived++;
                                total_packets_prod++;
				printf("\n %012.3lfms: Transmit packet %d to transmit queue\n",GetTimeOfDay(),transmit_packet->packet_num);
                                
                                transmit_packet->Q1_time_enters = GetTimeOfDay();
				add_item_list(&tx_queue, transmit_packet);
		        	printf("\n %012.3lfms: ------Packets sent  from the transmitter------\n",GetTimeOfDay());
				
				ready = true;

				/* Exit critical section */
        			pthread_mutex_unlock(&lock_queue);
			}
    		}
                else
                 {
                     printf("%012.3lfms: p%d arrives, needs %d tokens, inter-arrival time = %.3fms, dropped\n",GetTimeOfDay(),transmit_packet->packet_num,transmit_packet->tokens_reqd, inter_a_time);
                     packets_drop++;
                     total_packets_prod++;
                     free(transmit_packet);
                     pthread_mutex_unlock(&lock_queue);
            
                 }
  
    }	 
}

/************************************************************
Function    : receiver_thread 
 
Description : thread that recives packets an, processes and
              deletes packets from output queue

************************************************************/
void* receiver_thread()
{
    printf("\n %012.3lfms: Receiver_thread created\n", GetTimeOfDay());
    double time_in_system = 0.0;
    
while(!cntrl_c_signal)
    {
     pthread_mutex_lock(&lock_queue);
     /* critical section */

     if (isListEmpty(&rx_queue))
     {
       printf("\n %012.3lfms: Receiver Queue is empty, Wait for packets to arrive \n",GetTimeOfDay());
       pthread_cond_wait(&cond_wait, &lock_queue);
     }
       
       s_packet *packet_info = remove_element_first(&rx_queue);
       packet_info->total_Q2_time = GetTimeOfDay() - packet_info->Q2_time_enters;
       packet_info->S_time_enters = GetTimeOfDay();
       printf("\n %012.3lfms: ------Packets Received at the Receiver------\n",GetTimeOfDay());
       printf("\n %012.3lfms: Removed Packet Number %d from receiver queue\n",GetTimeOfDay(), packet_info->packet_num);
       pthread_mutex_unlock(&lock_queue);
       usleep(packet_info->servicetime);
       pthread_mutex_lock(&lock_queue);
   /*number of packets served by the system*/
       packet_served++;
       packet_info->total_S_time = GetTimeOfDay() - packet_info->S_time_enters;
       time_in_system = GetTimeOfDay()- packet_info->Q1_time_enters;
       sys_variance += time_in_system * time_in_system ;

       total_ser_time += packet_info->total_S_time;
       total_sys_time += time_in_system;
       time_in_Q1 += packet_info->total_Q1_time;
       time_in_Q2 += packet_info->total_Q2_time;

       free(packet_info);
      
     pthread_mutex_unlock(&lock_queue);
    }
}

/************************************************************
Function    :parse_cmd_line 
 
Description : Parses command line and store the required
              arguments

************************************************************/

void parse_cmd_line(int argc, char *argv[])
{
	int i = 0; char *delim;
	for (i=1; i<argc; i+=2) // start from i=1, discard executable name
	{
                if(strcmp(argv[i],"-lambda") == 0)
		{	
			params.lambda = strtod(argv[i+1],&delim);
		}
		else if(strcmp(argv[i],"-mu") == 0)
		{
			params.mu = strtod(argv[i+1],&delim);
		}
		else if(strcmp(argv[i],"-rate") == 0)
		{
			params.rate = strtod(argv[i+1],&delim);
		}
		else if(strcmp(argv[i],"-depth") == 0)
		{
			params.depth = atoi(argv[i+1]);
		}
		else if(strcmp(argv[i],"-token") == 0)
		{
			params.tokens = atoi(argv[i+1]);
		}
                else
			printf("%012.3lfms: Wrong parameters\n",GetTimeOfDay());
	}
        printf("%012.3lfms: Emulation User input Parameters \n",GetTimeOfDay());
printf(" ____________________________________________________\n");
        printf(" | Arrival Time \t\t\t:: %.6g      |\n | Service time of packet \t\t:: %f |\n | Rate at which tokens generated \t:: %f |\n | Token depth \t\t\t\t:: %d       |\n | Number of tokens \t\t\t:: %d        |\n",
			params.lambda,
			params.mu,
			params.rate,
			params.depth,
			params.tokens);
printf(" ____________________________________________________\n");

}
/************************************************************
Function    : main
 
Description : Parses command line, creates threads and queues

************************************************************/

int main(int argc,char *argv[])
{
   printf("\n\n***** Emulate token bucket algorithm *****\n\n");
 
   parse_cmd_line(argc,argv);

    sigemptyset(&intSignal);
    sigaddset(&intSignal, SIGINT);
    pthread_sigmask(SIG_BLOCK, &intSignal, 0);

   InitialCurTimeOfDay();
   
   printf("00000000.000ms: emulation begins\n");

   //pthread creation
   int res = pthread_create(&token_threadId, NULL, &token_thread,NULL);
   if (res != 0)
   {
	   printf("token thread creation failed\n");
   }
   res = pthread_create(&transmitter_threadId, NULL, &transmitter_thread,NULL);
   if (res != 0)
   {
	   printf(" transmitter thread creation failed\n");
   }
   res = pthread_create(&receiver_threadId, NULL, &receiver_thread,NULL);
   if (res != 0)
   {
	   printf("receiver thread creation failed\n");
   }
   res = pthread_create(&signalHandle, NULL, interrupt_handler, NULL);
   if (res != 0)
   {
	   printf("signalHandler thread creation failed\n");
   }
   pthread_join( token_threadId, NULL);
   pthread_join( transmitter_threadId, NULL);
   pthread_join( receiver_threadId, NULL);
   
   pthread_mutex_unlock(&lock_queue);
   emulation_ends = GetTimeOfDay();
   printf("Emulation ends %012.3lfms: \n ",emulation_ends);

   PrintStatistics();
   printf("-------------Token bucket Algorithm successfully emulated-----------------\n\n\n");
   return 1;

}